from django.shortcuts import render, redirect
from tasks.models import Task
from tasks.forms import Task_form
from django.contrib.auth.decorators import login_required


def create_task(request):
    if request.method == "POST":
        form = Task_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = Task_form()

    context = {"form": form}

    return render(request, "tasks/create.html", context)


@login_required
def show_my_tasks(request):
    model_instance = Task.objects.filter(assignee=request.user)
    context = {"task": model_instance}
    return render(request, "tasks/list.html", context)
