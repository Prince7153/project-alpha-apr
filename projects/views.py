from django.shortcuts import render, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import project_form


# Create your views here.
@login_required
def list_projects(request):
    projects = Project.objects.filter(owner=request.user)
    context = {
        "projects": projects,
    }
    return render(request, "projects/list.html", context)


def show_project(request, id):
    model_instance = Project.objects.get(id=id)
    context = {"project": model_instance}
    return render(request, "projects/detail.html", context)


def create_project(request):
    if request.method == "POST":
        form = project_form(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = project_form()

    context = {"form": form}

    return render(request, "projects/create.html", context)
